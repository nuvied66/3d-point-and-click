﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Hotspot : MonoBehaviour
{
	public Transform target;
	Transform camTransform;
	// Use this for initialization
	void Start ()
	{
		camTransform = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnMouseDown ()
	{
		camTransform.DOMove (target.position, 2f);
		camTransform.DORotate (camTransform.root.eulerAngles, 2f);
		//target.GetComponent<Camera> ().orthographicSize;
	}
}
